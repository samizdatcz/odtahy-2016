require! {
  fs
  async
  diacritics
}

require! "csv-parse":csvParse

reader = csvParse(delimiter: ',')

file = "mp"
targetDir = "mp"

stream = fs.createReadStream "#__dirname/../data/#file.csv"
stream.pipe reader

isRychlost = 'rychlost' is targetDir.split '-' .1
out = {}
typIndices = {}
currentTypIndex = 0
addresses = {}
reader.on \data (line) ->
  if 'mp' == file
    [x, y, spachano, addr, typ] = line
    spachano .= replace /[^0-9]/g ''
  else
    [x, y, spachano, time, addr] = line
    typ = "Neznámý"
    [d, m, yr] = spachano.split "."
    [h] = time.split ":"
    spachano = "#{yr}#{m}#{d}#{h}"
  return if x == 'X'
  x = parseFloat x
  # x -= 0.0011
  y = parseFloat y
  # y -= 0.00074
  return unless x > 0 and y > 0
  x .= toFixed 5
  y .= toFixed 5
  # typ = diacritics.remove typ
  typ .= toLowerCase!
  typ .= replace /[^a-z0-9]/gi ''
  typ .= replace /s/g 'z'
  typId = if typIndices[typ]
    that
  else
    currentTypIndex++
    i = currentTypIndex
    typIndices[typ] = i
    i
  id = [x, y].join "\t"
  if !isRychlost or -1 != typ.indexOf 'rychlozt'
  # if !isRychlost or typ.match /125codzt1pzmfbod[2-4]/
    out[id] = out[id] + 1 || 1
    addresses[id] ?= "#addr"

<~ reader.on \end
output = for id, count of out
  line = id + "\t#count"
  if isRychlost then line += "\t#{addresses[id]}"
  line

console.log "writing #{output.length} lines"
output.unshift unless isRychlost then "x\ty\tcount" else "x\ty\tcount\taddress"
<~ fs.writeFile "#__dirname/../data/processed/#targetDir/grouped.tsv" output.join "\n"
